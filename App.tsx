// Alguns imports estão sendo feitos para adicionar as fontes ao trabalho :)
import { useFonts,Montserrat_100Thin,Montserrat_200ExtraLight,Montserrat_300Light,Montserrat_400Regular,Montserrat_500Medium,
  Montserrat_600SemiBold,Montserrat_700Bold,Montserrat_800ExtraBold,} from '@expo-google-fonts/montserrat';
import { StatusBar } from 'expo-status-bar';
import AppLoading from "expo-app-loading";
import { StyleSheet, Text, View } from 'react-native';
import Login from './src/pages/Login';
import Welcome from './src/pages/Welcome';

export default function App() {
  let [fontsLoaded] = useFonts({
    Montserrat_100Thin,
    Montserrat_200ExtraLight,
    Montserrat_300Light,
    Montserrat_400Regular,
    Montserrat_500Medium,
    Montserrat_600SemiBold,
    Montserrat_700Bold,
    Montserrat_800ExtraBold,
  });
  if (!fontsLoaded) {
    return <AppLoading />
  }
  return (
    <View >
      <Login/>
    </View>
  );
}