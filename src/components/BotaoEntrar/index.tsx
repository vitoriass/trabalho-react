import React from "react";
import { Text, TouchableOpacity, View, StyleSheet } from "react-native";


const BotaoEntrar = ()=> {
    


    return(
        <TouchableOpacity>
            <View style={styles.btn}>
                <Text style={styles.txt}> Entrar</Text>
            </View>
        </TouchableOpacity>

    )

}
const styles=StyleSheet.create({

    btn:{
        width:293,
        height:55,
        backgroundColor:"#FFCD00",
        borderRadius:10,
        marginLeft:67,
        alignItems:"center",
        justifyContent:"center"
    },

    txt:{
        fontSize:20,
        color:"#2B3151",
        letterSpacing:1.3,

    },
})
export default BotaoEntrar;