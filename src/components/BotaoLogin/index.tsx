import React from "react";
import { Text, TouchableOpacity, View, StyleSheet } from "react-native";


const BotaoEntrar = ()=> {

    return(
        <TouchableOpacity>
            <View style={styles.btn}>
                <Text style={styles.txt}> Entrar</Text>
            </View>
        </TouchableOpacity>

    )

}
const styles=StyleSheet.create({

    btn:{
        width:335,
        height:52,
        backgroundColor:"#0091E6",
        borderRadius:10,
        marginLeft:35,
        alignItems:"center",
        justifyContent:"center",
        marginBottom:34,

    },

    txt:{
        fontFamily: "montserrat",
        fontSize:20,
        color:"#FFFFFF",
        letterSpacing:1.3,
        textAlign:"center",

    },
})
export default BotaoEntrar;