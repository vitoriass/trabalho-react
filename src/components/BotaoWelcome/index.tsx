import React from "react";
import { Text, TouchableOpacity, View, StyleSheet } from "react-native";


const BotaoWelcome = ()=> {
    
    return(
        <TouchableOpacity>
            <View style={styles.btn}>
                <Text style={styles.txt}> Vamos lá! </Text>
            </View>
        </TouchableOpacity>

    )

}
const styles=StyleSheet.create({

    btn:{
        width:303,
        height:69,
        backgroundColor:"white",
        borderRadius:48,
        marginLeft:67,
        alignItems:"center",
        justifyContent:"center",
        marginBottom:34,
        marginTop:360,
    },

    txt:{
        fontWeight: "700",
        fontSize:20,
        color:"black",
        letterSpacing:1.3,

    },
})
export default BotaoWelcome;