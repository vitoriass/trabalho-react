import React from "react";
import { View, Text, TextInput, StyleSheet } from "react-native";

export default function InputTexto({param}){
    return(
        <View style={styles.container}>
            <TextInput style={styles.caixatexto} placeholder={param}/>
        </View>
    )
}

const styles=StyleSheet.create({

    container:{
    },

    caixatexto:{
        backgroundColor:'white',
        width: 333,
        height: 51,
        borderRadius:10,
        borderWidth: 1,
        border: "solid #E3E3EA",
        marginBottom:15,
        marginTop:5,
        fontSize:16,
        paddingLeft:15,
        color:"black",
        placeholderTextColor:"black",
    },
});