import React from "react";
import BotaoEntrar from "../../components/BotaoLogin";
import InputTexto from "../../components/InputTexto";
import {Container, FundoApp, Logo, Embaixo, Dados1, Dados2, TxtEmb1, TxtEmb2, Texto1, Texto2, Icones, Icone1, Icone2, LogoImg} from "../Login/style";

const Login = ()=> {
    return (
        <Container>
            {/* todos os itens/objetos estão sobre a imagem de fundo */}
            <FundoApp 
                source={require('../../assets/BackGround.png')} > 
                {/* imagem da logo Reus-e */}
                <Logo>
                    <LogoImg
                        source={require('../../assets/Reus-e.png')}/>
                </Logo>

                {/* forms de entrada dos dados do usuário */}
                <Dados1>
                    <Texto1> E-mail </Texto1>
                    <InputTexto param={"Digite seu e-mail"}/>
                </Dados1>
                <Dados2>
                    <Texto1> Senha </Texto1>
                    <InputTexto param={"Digite sua senha"}/>
                </Dados2>

                {/* textos embaixo dos forms */}
                <Embaixo>
                    <TxtEmb1> Esqueci minha senha </TxtEmb1>
                    <TxtEmb2> É novo por aqui? registre-se </TxtEmb2>
                </Embaixo>

                {/* botão para logar na conta*/}
                <BotaoEntrar />

                <Texto2> Ou entrar com </Texto2>

                {/* Icones para log in alternativo */}
                <Icones>
                    <Icone1
                        source={require('../../assets/google.png')}/>
                    <Icone2
                        source={require('../../assets/Facebook.png')}/>
                </Icones>

            </FundoApp>
        </Container>
    ) 
}

export default Login;