import styled from "styled-components/native";
import { style } from "../../global/global";

export const Container = styled.View`
  flex: 1;
  flex-direction: column;
  background: ${style.colors.purple};
  position: relative;
`;
export const FundoApp = styled.ImageBackground`
    width:428;
    height:926;
    display:flex;
    flex-direction:column;
    align-content:center;
`;
export const Logo = styled.View`
    padding-top:132;
    padding-left:54;
`;
export const LogoImg = styled.Image`
    width:310;
    height:70;
    Left:44;
`;
export const Embaixo = styled.View`
    flex-direction: row;
    justify-content: center;
    margin-top:12;
    margin-bottom:24;
`;
export const Dados1 = styled.View`
    margin-top:125;
    margin-left:40;
`;
export const Dados2 = styled.View`
    margin-left:40;
`;
export const TxtEmb1 = styled.View`
    color: black;
    margin-left: 25;
    font-family:${style.fonts.mnsrt};
    font-size:13;
`;
export const TxtEmb2 = styled.View`
    color: black;
    padding-horizontal: 30;
    font-family:${style.fonts.mnsrt};
    font-size:13;
`;
export const Texto1 = styled.View`
    font-size:16;
    color: black;
    font-family:${style.fonts.mnsrt};
`;
export const Texto2 = styled.View`
    flex-direction: row;
    margin-left:155;
    margin-bottom:20;
    font-family:${style.fonts.mnsrt};
`;
export const Icones = styled.View`
    flex-direction: row;
    align-items: center;
    margin-left:100;
`;
export const Icone1 = styled.Image`
    height: 52.500003814697266;
    width: 53.125;
    left: 12;
`;
export const Icone2 = styled.Image`
    height: 67;
    width: 67;
    left: 100;
`;