import React from "react";
import {Container, FundoWelcome, MarcadorPagina,Logo,LogoImg} from "../Welcome/style";
import BotaoWelcome from "../../components/BotaoWelcome";


const Welcome = ()=> {
    return (
        <Container>
            {/* imagem da logo Reus-e */}
            <Logo>
                <LogoImg
                    source={require('../../assets/Reus-e.png')}/>
            </Logo>

            {/* corpo da página */}
            <FundoWelcome source={require('../../assets/BackGroundWelcome.png')} >
                {/* botão para iniciar o app */}
                <BotaoWelcome/>
            </FundoWelcome>

            {/* bolinhas que marcam a página */}
            <MarcadorPagina source={require('../../assets/Group 5.png')}/>
            
        </Container>
    )
}

export default Welcome;