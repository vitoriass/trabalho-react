import styled from "styled-components/native";
import { style } from "../../global/global";

export const Container = styled.View`
    background: ${style.colors.purple};
    width:428;
    height:926;
`;
export const FundoWelcome = styled.ImageBackground`
    width:428;
    height:650;
    display: flex;
    flex-direction: column;
    align-content: center;
`;
export const MarcadorPagina = styled.Image`
    width:38;
    height:8;
    margin-left:195;
`;
export const Logo = styled.View`
    padding-top:150;
    padding-left:54;
`;
export const LogoImg = styled.Image`
    width:310;
    height:70;
    Left:44;
`;